// PlayerClient.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "../Common/Game.h"

int _tmain(int argc, _TCHAR* argv[])
{
	int team = argc > 1 ? _tstoi(argv[1]) : 1;

	Game game(team);
	while (game.IsRunning())
	{
		game.GameLoop();
	}
	return 0;
}