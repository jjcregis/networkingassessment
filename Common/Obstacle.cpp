#include "stdafx.h"
#include "Obstacle.h"
#include "Game.h"
#include "Utils.h"

Obstacle::Obstacle(Game& game, int team) :
	_game(game),
	_velocity(sf::Vector2f(250, 200))
{
	_team = team;
	_id = team * 100;
	_removeIfOffScreen = false;

	LoadSprite();
}


Obstacle::~Obstacle()
{
}

void Obstacle::Draw(sf::RenderWindow & rw)
{
	GameObject::Draw(rw);
}

void Obstacle::LoadSprite()
{
	// Load the obstacle sprite
	std::string spritePath;
	switch (_team)
	{
	case 1:
		spritePath = "images/Obstacle1.png";
		break;
	case 2:
		spritePath = "images/Obstacle2.png";
	default:
		break;
	}
	Load(spritePath);
	assert(IsLoaded());
	GetSprite().setOrigin(GetSprite().getLocalBounds().width / 2, GetSprite().getLocalBounds().height / 2);
}

sf::Vector2f Obstacle::GetVelocity() const
{
	return _velocity;
}

void Obstacle::SetVelocity(float x, float y)
{
	_velocity = sf::Vector2f(x, y);
}

void Obstacle::Update(float elapsedTime)
{
	// Bounce off screen edges instead of going out of bounds
	sf::Vector2f position = GetPosition();
	float radius = GetRadius();

	if (position.x - radius < 0)
	{
		_velocity.x = abs(_velocity.x);
	}
	if (position.x + radius > _game.SCREEN_WIDTH)
	{
		_velocity.x = -abs(_velocity.x);
	}
	if (position.y - radius < 0)
	{
		_velocity.y = abs(_velocity.y);
	}
	if (position.y + radius > _game.SCREEN_HEIGHT)
	{
		_velocity.y = -abs(_velocity.y);
	}
	
	GetSprite().move(_velocity.x * elapsedTime, _velocity.y * elapsedTime);
}

sf::Packet& operator <<(sf::Packet& packet, const Obstacle& o)
{
	sf::Vector2f position = o.GetPosition();
	return packet << o._team << position.x << position.y << o._velocity.x << o._velocity.y;
}

sf::Packet& operator >>(sf::Packet& packet, Obstacle& o)
{
	sf::Vector2f position;
	int team;

	sf::Packet& ref = packet >> team >> position.x >> position.y >> o._velocity.x >> o._velocity.y;
	
	o.SetTeam(team);
	o.SetPosition(position.x, position.y);

	return ref;
}
