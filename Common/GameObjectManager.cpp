#include "stdafx.h"
#include "GameObjectManager.h"
#include "GameObject.h"
#include "Network.h"
#include "Game.h"

// Network Objects
#include "Player.h"
#include "Obstacle.h"
#include "Bullet.h"

GameObjectManager::GameObjectManager(Game& game) :
_game(game)
{
}

GameObjectManager::~GameObjectManager()
{
	// Delete all objects
	std::for_each(_gameObjects.begin(), _gameObjects.end(), GameObjectDeallocator());
}

void GameObjectManager::Add(int id, GameObject* gameObject)
{
	// Check if the object is already in the map. If so, do nothing
	GameObject* objectInMap = _gameObjects[id];

	// Do not add the object if it exists in the map
	if (objectInMap != NULL && gameObject == objectInMap)
	{
		return;
	}

	// Using [] operator inserts an empty object if the id isn't found. Delete it so we can insert the real object.
	if (objectInMap == NULL)
	{
		_gameObjects.erase(id);
	}

	// Find the next empty slot in the map to store the new object
	while (objectInMap != NULL)
	{
		objectInMap = _gameObjects[id];

		// Using [] operator inserts an empty object if the id isn't found. Delete it so we can insert the real object.
		if (objectInMap == NULL)
		{
			_gameObjects.erase(id);
			break;
		}

		id++;
	}

	// Update the object with it's new ID and store in the map.
	gameObject->SetId(id);
	_gameObjects.insert(std::pair<int, GameObject*>(id, gameObject));

}

void GameObjectManager::Remove(int id)
{
	std::map<int, GameObject*>::iterator results = _gameObjects.find(id);
	if (results != _gameObjects.end())
	{
		delete results->second;
		_gameObjects.erase(results);
	}
}

void GameObjectManager::RemoveTeam(int team)
{
	std::vector<int> idsToDelete;

	// Find all the IDs of the objects to delete
	std::map<int, GameObject*>::iterator itr = _gameObjects.begin();
	while (itr != _gameObjects.end())
	{
		if (itr->second->GetTeam() == team)
		{
			idsToDelete.push_back(itr->first);
		}
		itr++;
	}
	
	// Delete the objects
	for (int i = 0; i < idsToDelete.size(); i++)
	{
		std::map<int, GameObject*>::iterator result = _gameObjects.find(idsToDelete[i]);
		delete result->second;
		_gameObjects.erase(result);
	}
}

GameObject* GameObjectManager::Get(int id) const
{
	std::map<int, GameObject*>::const_iterator results = _gameObjects.find(id);
	if (results == _gameObjects.end())
		return NULL;
	return results->second;

}

int GameObjectManager::GetObjectCount() const
{
	return _gameObjects.size();
}


void GameObjectManager::DrawAll(sf::RenderWindow& renderWindow)
{
	std::map<int, GameObject*>::const_iterator itr = _gameObjects.begin();
	while (itr != _gameObjects.end())
	{
		itr->second->Draw(renderWindow);
		itr++;
	}
}

void GameObjectManager::UpdateAll(float frameTime)
{
	std::map<int, GameObject*>::const_iterator itr =
		_gameObjects.begin();

	while (itr != _gameObjects.end())
	{
		GameObject* obj = itr->second;

		// Update object
		obj->Update(frameTime);

		// Delete if it's offscreen
		sf::Vector2f position = obj->GetPosition();
		if (obj->GetRemoveIfOffScreen() &&
			(position.x < 0 ||
			position.x > _game.SCREEN_WIDTH ||
			position.y < 0 ||
			position.y > _game.SCREEN_HEIGHT))
		{
			itr = _gameObjects.erase(itr);
		}
		else
		{
			itr++;
		}
	}
}

sf::Packet& operator <<(sf::Packet& packet, const GameObjectManager& g)
{
	std::map<int, GameObject*>::const_iterator itr = g._gameObjects.begin();
	while (itr != g._gameObjects.end())
	{
		// Only send own updates
		if (itr->second->GetTeam() == g._game.GetTeam())
		{
			if (dynamic_cast<Player*>(itr->second) != NULL)
			{
				Player* player = dynamic_cast<Player*>(itr->second);
				packet << PlayerMessage << itr->first << *player;
			}
			else if (dynamic_cast<Obstacle*>(itr->second) != NULL)
			{
				Obstacle* obstacle = dynamic_cast<Obstacle*>(itr->second);
				packet << ObstacleMessage << itr->first << *obstacle;
			}
			else if (dynamic_cast<Bullet*>(itr->second) != NULL)
			{
				Bullet* bullet = dynamic_cast<Bullet*>(itr->second);
				packet << BulletMessage << itr->first << *bullet;
			}
		}
		itr++;
	}
	return packet;
}

sf::Packet& operator >>(sf::Packet& packet, GameObjectManager& g)
{
	int messageType;
	int id;
	GameObject* gameObject;

	while (!packet.endOfPacket())
	{
		packet >> messageType >> id ;
		gameObject = g.Get(id);

		switch ((MessageType)messageType)
		{
		case PlayerMessage:
		{
			Player* player = dynamic_cast<Player*>(gameObject);
			if (gameObject == NULL)
			{
				player = new Player(g._game);
			}
			if (player != NULL)
			{
				packet >> *player;
				g.Add(id, player);
			}
			break;
		}
		case ObstacleMessage:
		{
			Obstacle* obstacle = dynamic_cast<Obstacle*>(gameObject);
			if (gameObject == NULL)
			{
				obstacle = new Obstacle(g._game);
			}
			if (obstacle != NULL)
			{
				packet >> *obstacle;
				g.Add(id, obstacle);
			}
			break;
		}
		case BulletMessage:
		{
			Bullet* bullet = dynamic_cast<Bullet*>(gameObject);
			if (gameObject == NULL)
			{
				bullet = new Bullet(g._game);
			}
			if (bullet != NULL)
			{
				packet >> *bullet;
				g.Add(id, bullet);
			}
			break;
		}
		default:
			break;
		}		
	}

	return packet;
}