#include "stdafx.h"
#include "GameObject.h"
#include <cstdlib>

GameObject::GameObject() :
_isLoaded(false),
_isControllable(false),
_isSimulated(false),
_removeIfOffScreen(true)
{
}

GameObject::~GameObject()
{
}

void GameObject::Load(std::string filename)
{
	// Load and set the object's sprite
	if (_image.loadFromFile(filename) == false)
	{
		_filename = "";
		_isLoaded = false;
	}
	else
	{
		_filename = filename;
		_sprite.setTexture(_image);
		_isLoaded = true;
	}
}

void GameObject::Draw(sf::RenderWindow & renderWindow)
{
	if (_isLoaded)
	{
		renderWindow.draw(_sprite);
	}
}

void GameObject::Update(float elapsedTime)
{
}

void GameObject::SetPosition(float x, float y)
{
	if (_isLoaded)
	{
		_sprite.setPosition(x, y);
	}
}

sf::Vector2f GameObject::GetPosition() const
{
	if (_isLoaded)
	{
		return _sprite.getPosition();
	}
	return sf::Vector2f();
}

sf::Sprite& GameObject::GetSprite()
{
	return _sprite;
}

bool GameObject::IsLoaded() const
{
	return _isLoaded;
}

int GameObject::GetId() const
{
	return _id;
}

void GameObject::SetId(int id)
{
	_id = id;
}

int GameObject::GetTeam() const
{
	return _team;
}

void GameObject::SetTeam(int team)
{
	_team = team;
	LoadSprite();
}

float GameObject::GetRadius() const
{
	return _image.getSize().x / 2;
}

bool GameObject::GetRemoveIfOffScreen() const
{
	return _removeIfOffScreen;
}

bool GameObject::GetIsControllable() const
{
	return _isControllable;
}

void GameObject::SetIsControllable(bool controllable)
{
	_isControllable = controllable;
}