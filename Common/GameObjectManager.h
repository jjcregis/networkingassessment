#pragma once
#include "GameObject.h"
#include <queue>

class Game;

class GameObjectManager
{
public:
	GameObjectManager(Game& game);
	~GameObjectManager();

	void Add(int id, GameObject* gameObject);
	void Remove(int id);
	void RemoveTeam(int team);
	int GetObjectCount() const;
	GameObject* Get(int id) const;

	void DrawAll(sf::RenderWindow& renderWindow);
	void UpdateAll(float frameTime);

	friend sf::Packet& operator<<(sf::Packet& packet, const GameObjectManager& m);
	friend sf::Packet& operator>>(sf::Packet& packet, GameObjectManager& m);

private:
	Game& _game;
	std::map<int, GameObject*> _gameObjects;

	struct GameObjectDeallocator
	{
		void operator()(const std::pair<int, GameObject*> & p) const
		{
			delete p.second;
		}
	};
};