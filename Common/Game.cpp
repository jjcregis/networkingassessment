#include "stdafx.h"
#include "Game.h"
#include "Bullet.h"

#include "GameObject.h"
#include "Player.h"
#include "Bullet.h"
#include "Obstacle.h"

#include "Utils.h"
#include "Network.h"

// For files
#include <iostream>
#include <fstream>

Game::Game(int team) :
_isRunning(true),
_networkUpdateInterval(.1f)
{
	// Create the object manager
	_gameObjectManager = new GameObjectManager(*this);

	//Create the window
	_mainWindow.create(sf::VideoMode(SCREEN_WIDTH, SCREEN_HEIGHT, 32), "");

	// Seed random number generator
	srand(time(0));

	// Read the connection info file
	std::ifstream file("Data/IP.txt");
	std::string str;
	if (file.is_open())
	{
		PlayerInfo info;

		for (int i = 0; i < 2; i++)
		{		
			// Read the player index
			std::getline(file, str);
			info.Team = (int)std::strtoul(str.c_str(), NULL, 0);

			// Read the IP
			std::getline(file, str);
			info.PlayerIp = str;

			// Read the receive port
			std::getline(file, str);
			info.ReceivePort = (unsigned short)std::strtoul(str.c_str(), NULL, 0);

			// Read the send port
			std::getline(file, str);
			info.SendPort = (unsigned short)std::strtoul(str.c_str(), NULL, 0);
			
			if (info.Team == team)
			{
				_myInfo = info;
			}
			else
			{
				_enemyInfo = info;
			}
		}
	}

	// Set the window title
	std::string windowTitle = _myInfo.Team == 1 ? "Red Client" : "Blue Client";
	_mainWindow.setTitle(windowTitle);

	// Set sockets to be non-blocking
	_sendSocket.setBlocking(false);
	_receiveSocket.setBlocking(false);

	// Bind receive socket
	_receiveSocket.bind(_myInfo.ReceivePort);

	// Create our player
	Player *player = new Player(*this, _myInfo.Team);
	player->SetPosition((SCREEN_WIDTH / 2) + 50* _myInfo.Team, (SCREEN_HEIGHT / 2));
	player->SetIsControllable(true);
	_gameObjectManager->Add(player->GetId(), player);
	_playerId = player->GetId();

	// Create our obstacle
	Obstacle *obstacle = new Obstacle(*this, _myInfo.Team);
	obstacle->SetPosition((SCREEN_WIDTH / 2) - 50 * _myInfo.Team, (SCREEN_HEIGHT / 2));
	_gameObjectManager->Add(obstacle->GetId(), obstacle);
}

Game::~Game()
{
	// Delete all game objects
	delete _gameObjectManager;

	// Unbind the receiving socket
	_receiveSocket.unbind();
}

void Game::SpawnBullet(int team, sf::Vector2f position, sf::Vector2f velocity)
{
	// Shoot a bullet
	Bullet* bullet = new Bullet(*this, team, velocity);
	bullet->SetPosition(position.x, position.y);
	_gameObjectManager->Add(bullet->GetId(), bullet);
}

int Game::GetTeam() const
{
	return _myInfo.Team;
}

float Game::GetLastEnemyUpdateTime()
{
	return _lastEnemyUpdateTime;
}

float Game::GetTimeSinceLastEnemyUpdate()
{
	return _enemyInfo.TimeoutCounter;
}

sf::Time Game::GetFrameTime()
{
	return _frameTime;
}

bool Game::IsRunning()
{
	return _isRunning;
}

bool Game::IsUsingPrediction()
{
	return _usePrediction;
}

sf::RenderWindow& Game::GetWindow()
{
	return _mainWindow;
}

void Game::GameLoop()
{
	// Poll for events
	sf::Event currentEvent;
	_mainWindow.pollEvent(currentEvent);

	// Get the duration of the last frame
	_frameTime = _clock.restart();

	// Keep track of total time
	_totalTime += _frameTime.asSeconds();

	// Stop running if the window closes or ESC is pressed
	if (currentEvent.type == sf::Event::Closed)
	{
		_isRunning = false;
		return;
	}
	_isRunning = !sf::Keyboard::isKeyPressed(sf::Keyboard::Escape);

	// Stop using dead-reckoning prediction when P is pressed
	_usePrediction = !sf::Keyboard::isKeyPressed(sf::Keyboard::P);

	// Receive network updates
	ReceiveNetworkUpdate();

	// Increase opponent's timeout counter
	_enemyInfo.TimeoutCounter += _frameTime.asSeconds();

	// Remove the opponent's objects if they've timed out
	if (_enemyInfo.TimeoutCounter > _timeOutTime)
	{
		_gameObjectManager->RemoveTeam(_enemyInfo.Team);
	}

	// Update all objects and draw them
	_mainWindow.clear(sf::Color(32, 32, 32));
	_gameObjectManager->UpdateAll(_frameTime.asSeconds());
	_gameObjectManager->DrawAll(_mainWindow);
	_mainWindow.display();	

	// Send a network update every ~100ms
	_timeSinceLastUpdate += _frameTime.asSeconds();
	if (_timeSinceLastUpdate >= _networkUpdateInterval)
	{
		_timeSinceLastUpdate -= _networkUpdateInterval;
		SendNetworkUpdate();
	}
}

void Game::SendNetworkUpdate()
{
	// Update players and objects
	sf::Packet packet;

	// Pack timestamp
	packet << _totalTime;

	// Pack object data
	packet << *_gameObjectManager;

	// Send packet
	sf::Socket::Status status = _sendSocket.send(packet, sf::IpAddress(_enemyInfo.PlayerIp), _enemyInfo.ReceivePort);
	if (status != sf::Socket::Done)
	{
		std::cout << "Failed to send";
	}
}

void Game::ReceiveNetworkUpdate()
{
	sf::Packet packet;

	// Receive packet from the other player
	if (_receiveSocket.receive(packet, sf::IpAddress(_enemyInfo.PlayerIp), _enemyInfo.SendPort) == sf::Socket::Done)
	{
		// Reset opponent's timeout counter
		_enemyInfo.TimeoutCounter = 0;

		// Uppack timestamp
		packet >> _lastEnemyUpdateTime;

		// Ensure that we are not updating the game state with old data
		if (_lastEnemyUpdateTime > _enemyInfo.LastUpdateTime)
		{
			_enemyInfo.LastUpdateTime = _lastEnemyUpdateTime;

			// Unpack the data from the player client to show in the spectator
			packet >> *_gameObjectManager;
		}
		else
		{
			// Discard the old packet
			packet.clear();
		}
	}
	else
	{
		// Nothing received
	}
}