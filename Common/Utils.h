#pragma once

class Utils
{
public:
	static float Clamp(float min, float max, float number);
	static float CubicInterpolate(float t, float v0, float v1, float v2, float v3);
};
