#pragma once

class GameObject
{
public:
	GameObject();
	virtual ~GameObject();

	virtual void Load(std::string filename);
	virtual void Draw(sf::RenderWindow & window);
	virtual void Update(float elapsedTime);

	virtual void SetPosition(float x, float y);
	virtual sf::Vector2f GetPosition() const;
	virtual bool IsLoaded() const;
	virtual void LoadSprite() = 0;
	float GetRadius() const;
	bool GetRemoveIfOffScreen() const;

	int GetId() const;
	void SetId(int id);

	int GetTeam() const;
	void SetTeam(int team);

	bool GetIsControllable() const;
	void SetIsControllable(bool controllable);

protected:
	sf::Sprite& GetSprite();

	int _id;
	int _team;
	sf::Sprite  _sprite;
	sf::Texture _image;
	std::string _filename;
	bool _isLoaded;

	bool _removeIfOffScreen;
	bool _isControllable;
	bool _isSimulated;
};