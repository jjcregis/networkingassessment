#pragma once
#include "GameObject.h"
#include <vector>

class Game;

class Player :
	public GameObject
{
protected:
	struct PreviousState
	{
		float Time;
		sf::Vector2f Position;
		sf::Vector2f Velocity;
		float Angle;
	};

public:
	Player(Game& game, int team = 1);
	~Player();

	void Update(float elapsedTime);
	void Draw(sf::RenderWindow& rw);
	void LoadSprite();

	sf::Vector2f GetVelocity() const;
	void SetVelocity(float x, float y);
	float GetAngle() const;
	void SetAngle(float angle);

	friend sf::Packet& operator<<(sf::Packet& packet, const Player& p);
	friend sf::Packet& operator>>(sf::Packet& packet, Player& p);

private:
	sf::Vector2f _velocity;
	sf::Vector2f _maxVelocity;
	float _angle;
	float _size;
	float _attackCooldown;
	Game& _game;

	std::vector<PreviousState> _previousStates;
	const int _stateHistorySize = 4;
};