#include "stdafx.h"
#include "Bullet.h"
#include "Utils.h"
#include "Game.h"

Bullet::Bullet(Game& game, int team, sf::Vector2f velocity) :
	_game(game),
	_velocity(sf::Vector2f(velocity.x * 250, velocity.y * 250))
{
	_team = team;
	_id = team * 100;

	// Delete if offscreen
	_removeIfOffScreen = true;

	LoadSprite();
}

Bullet::~Bullet()
{
}

void Bullet::Draw(sf::RenderWindow & rw)
{
	GameObject::Draw(rw);
}

void Bullet::LoadSprite()
{
	// Load the bullet sprite
	std::string spritePath;
	switch (_team)
	{
	case 1:
		spritePath = "images/Bullet1.png";
		break;
	case 2:
		spritePath = "images/Bullet2.png";
	default:
		break;
	}
	Load(spritePath);
	assert(IsLoaded());

	_size = GetSprite().getLocalBounds().width / 2;

	GetSprite().setOrigin(GetSprite().getLocalBounds().width / 2, GetSprite().getLocalBounds().height / 2);
}

sf::Vector2f Bullet::GetVelocity() const
{
	return _velocity;
}

void Bullet::Update(float elapsedTime)
{
	GetSprite().move(_velocity.x * elapsedTime, _velocity.y * elapsedTime);
}

sf::Packet& operator <<(sf::Packet& packet, const Bullet& b)
{
	sf::Vector2f position = b.GetPosition();
	return packet << b._team << position.x << position.y << b._velocity.x << b._velocity.y;
}

sf::Packet& operator >>(sf::Packet& packet, Bullet& b)
{
	sf::Vector2f position;
	int team;

	sf::Packet& ref = packet >> team >> position.x >> position.y >> b._velocity.x >> b._velocity.y;
	
	b.SetTeam(team);
	b.SetPosition(position.x, position.y);

	return ref;
}