#pragma once
#include <string.h>

enum MessageType
{
	PlayerMessage = 10,
	BulletMessage = 20,
	ObstacleMessage = 30
};

struct PlayerInfo
{
	int Team;
	float LastUpdateTime = 0;
	float TimeoutCounter = 0;
	std::string PlayerIp;
	unsigned short SendPort;
	unsigned short ReceivePort;
};