#pragma once
#include "GameObject.h"

class Game;

class Bullet :
	public GameObject
{
public:
	Bullet(Game& game, int team = 1, sf::Vector2f velocity = sf::Vector2f(0, 0));
	~Bullet();

	void Update(float elapsedTime);
	void Draw(sf::RenderWindow& rw);
	void LoadSprite();

	sf::Vector2f GetVelocity() const;

	friend sf::Packet& operator<<(sf::Packet& packet, const Bullet & b);
	friend sf::Packet& operator>>(sf::Packet& packet, Bullet& b);

private:
	Game& _game;
	sf::Vector2f _velocity;
	float _size;
};