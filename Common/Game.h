#pragma once
#include "GameObjectManager.h"
#include "Network.h"

class Game
{
public:
	const static int SCREEN_WIDTH = 684;
	const static int SCREEN_HEIGHT = 384;

	Game(int team);
	~Game();

	sf::RenderWindow& GetWindow();

	void GameLoop();
	bool IsRunning();
	bool IsUsingPrediction();
	void SpawnBullet(int team, sf::Vector2f position, sf::Vector2f velocity);
	int GetTeam() const;

	float GetLastEnemyUpdateTime();
	float GetTimeSinceLastEnemyUpdate();

	sf::Time GetFrameTime();

protected:
	
	void SendNetworkUpdate();
	void ReceiveNetworkUpdate();

	const float _timeOutTime = 7;

	sf::RenderWindow _mainWindow;

	GameObjectManager* _gameObjectManager;
	sf::Clock _clock;
	sf::Time _frameTime;
	float _totalTime;

	PlayerInfo _myInfo;
	PlayerInfo _enemyInfo;

	bool _isRunning;
	sf::UdpSocket _sendSocket;
	sf::UdpSocket _receiveSocket;

	float _networkUpdateInterval;
	float _timeSinceLastUpdate;
	float _lastEnemyUpdateTime;

	int _playerId;
	bool _usePrediction;
};
