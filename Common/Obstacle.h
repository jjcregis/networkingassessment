#pragma once
#include "GameObject.h"

class Game;

class Obstacle :
	public GameObject
{
public:
	Obstacle(Game& game, int team = 1);
	~Obstacle();

	void Update(float elapsedTime);
	void Draw(sf::RenderWindow& rw);
	void LoadSprite();

	sf::Vector2f GetVelocity() const;
	void SetVelocity(float x, float y);

	friend sf::Packet& operator<<(sf::Packet& packet, const Obstacle& o);
	friend sf::Packet& operator>>(sf::Packet& packet, Obstacle& o);

private:
	sf::Vector2f _velocity;
	Game& _game;
};