#include "stdafx.h"
#include "Player.h"
#include "Game.h"
#include "Utils.h"

Player::Player(Game& game, int team) :
	_game(game),
	_velocity(sf::Vector2f(0, 0)),
	_maxVelocity(sf::Vector2f(250, 250)),
	_angle(0),
	_attackCooldown(0)
{
	_team = team;
	_id = team * 100;

	// Keep the player alive if it accidentally goes offscreen
	_removeIfOffScreen = false;

	LoadSprite();
}

Player::~Player()
{
}

void Player::Draw(sf::RenderWindow & rw)
{
	GameObject::Draw(rw);
}

void Player::LoadSprite()
{
	// Load the player sprite
	std::string spritePath;
	switch (_team)
	{
	case 1:
		spritePath = "images/Player1.png";
		break;
	case 2:
		spritePath = "images/Player2.png";
	default:
		break;
	}
	Load(spritePath);
	assert(IsLoaded());
	_size = GetSprite().getLocalBounds().width / 2;
	GetSprite().setOrigin(GetSprite().getLocalBounds().width / 2, 48);
}

sf::Vector2f Player::GetVelocity() const
{
	return _velocity;
}

void Player::SetVelocity(float x, float y)
{
	_velocity = sf::Vector2f(x, y);
}

float Player::GetAngle() const
{
	return _angle;
}

void Player::SetAngle(float angle)
{
	_angle = angle;
}

void Player::Update(float elapsedTime)
{
	GameObject::Update(elapsedTime);

	if (_isControllable)
	{
		// Reduce attack cooldown
		_attackCooldown = Utils::Clamp(0, 60, _attackCooldown - elapsedTime);

		// Update the player's angle to point to the mouse cursor
		sf::Vector2i mousePosition = sf::Mouse::getPosition(_game.GetWindow());
		sf::Vector2i playerPosition = sf::Vector2i((int)GetPosition().x, (int)GetPosition().y);
		_angle = atan2(mousePosition.x - playerPosition.x, playerPosition.y - mousePosition.y) * 180 / 3.14159f;

		// Handle movement input
		bool isMovingX = false;
		bool isMovingY = false;
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
		{
			_velocity.x -= 2.0f;
			isMovingX = true;
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
		{
			_velocity.x += 2.0f;
			isMovingX = true;
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
		{
			_velocity.y -= 3.0f;
			isMovingY = true;
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
		{
			_velocity.y += 3.0f;
			isMovingY = true;
		}
		
		// Handle attacking input
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
		{
			if (_attackCooldown <= 0)
			{
				// Shoot bullet
				_game.SpawnBullet(_team, GetPosition(), sf::Vector2f(sin(_angle * 3.14159f / 180), cos(_angle * 3.14159f / 180) * -1));
				_attackCooldown += 1;
			}
		}

		// Decelerate if not moving in a direction
		if (!isMovingX)
		{
			_velocity.x *= .995f;
		}
		if (!isMovingY)
		{
			_velocity.y *= .995f;
		}

		// Cap the player's velocity
		_velocity.x = Utils::Clamp(-_maxVelocity.x, _maxVelocity.x, _velocity.x);
		_velocity.y = Utils::Clamp(-_maxVelocity.y, _maxVelocity.y, _velocity.y);

		// Bounce off screen edges instead of going out of bounds
		sf::Vector2f position = GetPosition();
		float radius = GetRadius();

		if (position.x - radius < 0)
		{
			_velocity.x = abs(_velocity.x);
		}
		if (position.x + radius > _game.SCREEN_WIDTH)
		{
			_velocity.x = -abs(_velocity.x);
		}
		if (position.y - radius < 0)
		{
			_velocity.y = abs(_velocity.y);
		}
		if (position.y + radius > _game.SCREEN_HEIGHT)
		{
			_velocity.y = -abs(_velocity.y);
		}

		GetSprite().move(_velocity.x * elapsedTime, _velocity.y * elapsedTime);
	}
	else
	{
		// This player is not controlled by the player, so predict its movement.
		if (_game.IsUsingPrediction())
		{
			if (_previousStates.size() == 4)
			{
				// Use the last four updates to get three update intervals to estimate a fourth interval
				// Estimate values over this fourth interval.
				// State 0 = oldest, 3 = newest

				float futureIntervalLength = ((_previousStates[1].Time - _previousStates[0].Time) +
					(_previousStates[2].Time - _previousStates[1].Time) +
					(_previousStates[3].Time - _previousStates[2].Time)) / 3;
				float futureUpdateTime = _game.GetLastEnemyUpdateTime() + futureIntervalLength;
				float currentTime = _game.GetLastEnemyUpdateTime() + _game.GetTimeSinceLastEnemyUpdate();
				float t = Utils::Clamp(0, 1, (currentTime - _game.GetLastEnemyUpdateTime()) / (futureUpdateTime - _game.GetLastEnemyUpdateTime()));

				// Perform cubic interpolation on player velocity
				SetVelocity(Utils::CubicInterpolate((.75f) + (t / 4),
					_previousStates[1].Velocity.x,
					_previousStates[2].Velocity.x,
					_previousStates[3].Velocity.x,
					_previousStates[3].Velocity.x),
					Utils::CubicInterpolate((.75f) + (t / 4),
					_previousStates[1].Velocity.y,
					_previousStates[2].Velocity.y,
					_previousStates[3].Velocity.y,
					_previousStates[3].Velocity.y));
					
				float newAngle = (_previousStates[3].Angle * t) + (_previousStates[2].Angle) * (1 - t);
				SetAngle(newAngle);
			}

			// Bounce off screen edges instead of going out of bounds
			sf::Vector2f position = GetPosition();
			float radius = GetRadius();

			if (position.x - radius < 0)
			{
				_velocity.x = abs(_velocity.x);
			}
			if (position.x + radius > _game.SCREEN_WIDTH)
			{
				_velocity.x = -abs(_velocity.x);
			}
			if (position.y - radius < 0)
			{
				_velocity.y = abs(_velocity.y);
			}
			if (position.y + radius > _game.SCREEN_HEIGHT)
			{
				_velocity.y = -abs(_velocity.y);
			}

			GetSprite().move(_velocity.x * elapsedTime, _velocity.y * elapsedTime);
		}
	}

	// Update the image's angle and position
	GetSprite().setRotation(_angle);

}

sf::Packet& operator <<(sf::Packet& packet, const Player& p)
{
	sf::Vector2f position = p.GetPosition();
	return packet << p._team << position.x << position.y << p._velocity.x << p._velocity.y << p._angle;
}

sf::Packet& operator >>(sf::Packet& packet, Player& p)
{
	sf::Vector2f position;
	int team;

	sf::Packet& ref = packet >> team >> position.x >> position.y >> p._velocity.x >> p._velocity.y >> p._angle;
	
	p.SetTeam(team);
	p.SetPosition(position.x, position.y);

	// Store the old state
	Player::PreviousState state;
	state.Time = p._game.GetLastEnemyUpdateTime();
	state.Velocity = p._velocity;
	state.Angle = p._angle;
	state.Position = p.GetPosition();

	// Limit the history size
	if (p._previousStates.size() >= p._stateHistorySize)
	{
		p._previousStates.erase(p._previousStates.begin());
	}

	p._previousStates.push_back(state);

	return ref;
}