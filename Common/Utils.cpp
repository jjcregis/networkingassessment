#include "stdafx.h"
#include "Utils.h"

float Utils::Clamp(float min, float max, float number)
{
	if (number < min)
	{
		return min;
	}
	else if (number > max)
	{
		return max;
	}
	else return number;
}

//http://homepage.ttu.edu.tw/jmchen/gameprog/slides/reduce-lag-cubic-spline.pdf
float Utils::CubicInterpolate(float t, float v0, float v1, float v2, float v3)
{	
	float a, b, c, d;
	a = v3 - 3 * v2 + 3 * v1 - v0;
	b = 3 * v2 - 6 * v1 + 3 * v0;
	c = 3 * v1 - 3 * v0;
	d = v0;
	
	return a*pow(t, 3) + b*pow(t, 2) + c*t + d;
}